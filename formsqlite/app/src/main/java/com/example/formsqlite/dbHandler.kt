package com.example.formsqlite

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.ContactsContract
import android.widget.Toast


//fondamentals
val database_name = "MyDb"
val table_name = "Users"
val col_name = "Name"
val col_age = "Age"
val col_ID = "ID"

class dbHandler(context: Context) : SQLiteOpenHelper(context, database_name, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE "+ table_name + " ( " + col_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ col_name + " VARCHAR(256), "+ col_age +" INTEGER);"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //insert function
    fun insertdata(user: User){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(col_name, user.name)
        cv.put(col_age, user.age)
        db.insert(table_name, null, cv)

    }

    //read
    fun readdata() : MutableList<User>{
        var list : MutableList<User> = ArrayList()

        var db = this.readableDatabase
        val query = "Select * from " + table_name
        var result = db.rawQuery(query, null)
        if(result.moveToFirst()){
            do{
                var user = User()
                user.id = result.getString(result.getColumnIndex(col_ID)).toInt()
                user.name = result.getString(result.getColumnIndex(col_name))
                user.age = result.getString(result.getColumnIndex(col_age)).toInt()
                list.add(user)
            }while(result.moveToNext())
        }

        return list
    }

    fun deleteData(){
        val db = this.writableDatabase

        db.delete(table_name, null, null)
        db.close()
    }

    fun update(){
        var db = this.writableDatabase
        val query = "Select * from " + table_name
        var result = db.rawQuery(query, null)
        if(result.moveToFirst()){
            do{
                var cv = ContentValues()
                cv.put(col_age, result.getInt(result.getColumnIndex(col_age))+1)
                db.update(table_name, cv, col_ID+"=? AND "+ col_name+"=? AND "+ col_age+"=? ", arrayOf(result.getString(result.getColumnIndex(
                    col_ID)), result.getString(result.getColumnIndex(col_name))))
            }while(result.moveToNext())
        }
    }

}