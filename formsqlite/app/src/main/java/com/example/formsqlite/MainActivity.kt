package com.example.formsqlite

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var context = this
        var db = dbHandler(context)

        btnInsert.setOnClickListener({
            if(etvName.text.toString().length>0 && etvAge.text.toString().length>0){
                var user = User(etvName.text.toString(), etvAge.text.toString().toInt())
                db.insertdata(user)
                Toast.makeText(this, "Registered! Thank you :)", Toast.LENGTH_LONG).show()

            }
            else{
                Toast.makeText(this, "Please Fill the void", Toast.LENGTH_SHORT).show()
            }
        })

        btnRead.setOnClickListener({
            var data = db.readdata()
            tvResult.text = ""
            tvResult.append("Here your list of records: \n")
            for( i in 0..data.size - 1){
                tvResult.append(data.get(i).id.toString() + " " + data.get(i).name+ " " + data.get(i).age.toString() + "\n")
            }
        })

        btnUpdate.setOnClickListener({
            db.update()
            btnRead.performClick()
        })

        btnDelete.setOnClickListener({
            db.deleteData()
            btnRead.performClick()
        })
    }
}
